import React from 'react';
import {getCharacters} from "../../utils/axiosFunctions";
import Character from './Character/Character';
import './Characters.styles.scss';

class Characters extends React.Component {
    state = {
        characters: [],
        filterText: '',
        next: ''
    };

    componentDidMount() {
        this.loadMore();
    };

    async loadMore(next) {
        const data = await getCharacters(next);
        this.setState({
            characters: this.state.characters.concat(data.results),
            next: data.info.next
        })
    }

    changeFilterText(text) {
        this.setState({filterText: text.toLowerCase()});
    }

    render() {
        const filteredCharacterList = this.state.characters.filter(
            character => character.name.toLowerCase().includes(this.state.filterText));
        return (
            <div className='charactersContent'>
                <div className='flex'>
                    <input className='searchBar'
                           placeholder={'Enter character name...'}
                           onChange={e => this.changeFilterText(e.target.value)}
                    />
                </div>
                <div className='characterList'>
                    {filteredCharacterList.map((character, i) => {
                        return (
                            <Character character={character} key={i}/>);
                    })}
                </div>
                <div className='loadMoreButton'
                     onClick={() => this.loadMore(this.state.next)}
                >
                    {'Load More'}
                </div>
            </div>
        );
    }
}

export default Characters;
