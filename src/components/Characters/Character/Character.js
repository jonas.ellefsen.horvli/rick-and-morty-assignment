import React from 'react';
import './Character.styles.scss';
import {history} from '../../../history';

class Character extends React.Component {
    render() {
        const {character} = this.props;
        return (
            <div className='profileCard'>
                <div className='profileHeader'>
                    <img className='profileImage' src={character.image} alt={'profile.img'}/>
                    <div className='profileName'>{character.name}</div>
                </div>
                <div className='profileProperty'>
                    <div className='profilePropertyName'>Status</div>
                    <div className='profilePropertyValue'>{character.status}</div>
                </div>
                <div className='profileProperty'>
                    <div className='profilePropertyName'>Species</div>
                    <div className='profilePropertyValue'>{character.species}</div>
                </div>
                <div className='profileProperty'>
                    <div className='profilePropertyName'>Gender</div>
                    <div className='profilePropertyValue'>{character.gender}</div>
                </div>
                <div className='profileProperty profileBigProperty'>
                    <div className='profilePropertyName'>Origin</div>
                    <div className='profilePropertyValue'>{character.origin.name}</div>
                </div>
                <div className='profileProperty profileBigProperty'>
                    <div className='profilePropertyName'>Last location</div>
                    <div className='profilePropertyValue'>{character.location.name}</div>
                </div>
                <div className='viewProfileButton'
                     onClick={() => history.push(`/profile/${character.id}`)}>
                    View Profile
                </div>
            </div>
        );
    }
}

export default Character;
