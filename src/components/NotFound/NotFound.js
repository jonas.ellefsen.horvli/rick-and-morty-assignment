import React from 'react';
import './NotFound.styles.scss';
import {history} from "../../history";

const errorImage = require('../../Images/404Error.png');

function NotFound() {
    return (
        <div className='notFound'>
            <div className='backHomeButton'
                 onClick={() => history.push('/')}
            >Back Home</div>
            <img src={errorImage} alt={'404 not found error'} />
        </div>
    );
}

export default NotFound;
