import React from 'react';
import {getCharacter} from "../../utils/axiosFunctions";
import {history} from "../../history";
import './ProfileView.styles.scss';

class ProfileView extends React.Component {
    state = {profile: null};
    async componentDidMount() {
        const profile = await getCharacter(this.props.match.params.id);
        this.setState({profile});
    }

    render() {
        const { profile } = this.state;
        return (
            <div className='profileView'>
                <div className='backButton' onClick={() => history.push('/')}>Back</div>
            <header>
                {profile ?
                    <div className='profileViewContent'>
                        <header className='profileViewHeader'>
                            <img className='profileViewHeaderImage' src={profile.image} />
                            <div className='profileViewHeaderText'>
                                <h1>{profile.name}</h1>
                                <div>Status: {profile.status}</div>
                            </div>
                        </header>
                        <div className='profileInfoSection'>
                            <h2>Profile</h2>
                            <div className='profileInfo'>
                                <div className='propertyName'>Gender:</div>
                                <div className='propertyValue'>{profile.gender}</div>
                            </div>
                            <div className='profileInfo'>
                                <div className='propertyName'>Species:</div>
                                <div className='propertyValue'>{profile.species}</div>
                            </div>
                            <div className='profileInfo'>
                                <div className='propertyName'>Origin:</div>
                                <div className='propertyValue'>{profile.origin.name}</div>
                            </div>
                        </div>
                        <div className='profileInfoSection'>
                            <h2>Location</h2>
                            <div className='profileInfo'>
                                <div className='propertyName'>Current location:</div>
                                <div className='propertyValue'>{profile.location.name}</div>
                            </div>
                            <div className='profileInfo'>
                                <div className='propertyName'>Type:</div>
                                <div className='propertyValue'>{profile.type || 'planet'}</div>
                            </div>
                            <div className='profileInfo'>
                                <div className='propertyName'>Dimension:</div>
                                <div className='propertyValue'>{profile.origin.name}</div>
                            </div>
                        </div>
                    </div>
                    : <div>Loading</div>
                }
            </header>
        </div>);
    }
}

export default ProfileView;
