import React from 'react';
import './App.scss';
import ProfileView from "./components/ProfileView/ProfileView";
import Characters from "./components/Characters/Characters";
import {Route, Switch} from 'react-router-dom';
import NotFound from './components/NotFound/NotFound'

const banner = require('./Images/RickAndMortyBanner.jpg');

function App() {
  return (
    <div className="App">
      <header className="App-header">
          <div className='topBar'>
              <img className='banner' src={banner} alt={'banner.img'}/>
          </div>
      </header>
        <div className='content'>
            <Switch>
                <Route component={Characters} exact path={'/'}/>
                <Route component={ProfileView} exact path={'/profile/:id'}/>
                <Route component={NotFound} />
            </Switch>
        </div>
    </div>
  );
}

export default App;
