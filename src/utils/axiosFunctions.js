import Axios from "axios";
const apiUrl = 'https://rickandmortyapi.com/api';

export async function getCharacters(next = `${apiUrl}/character/`) {
    try {
        return (await Axios.get(next)).data;
    } catch (e) {
        console.error(`Failed to get characters: ${e.message}`)
    }
}

export async function getCharacter(id) {
    try {
        return (await Axios.get(`${apiUrl}/character/${id}`)).data;
    } catch (e) {
        console.error(`Failed to get character: ${e.message}`)
    }
}
