const express = require('express');
const path = require('path');
const app = express();

const _appFolder = path.join(__dirname, 'build');
app.use(express.static(_appFolder));

app.get('/', function (req, res) {
    res.sendFile(path.join(_appFolder, 'index.html'));
});

app.all('*', function (req, res) {
    res.status(200).sendFile(path.join(_appFolder, 'index.html'));
});

app.listen(process.env.PORT || 3000);
